#import <Cordova/CDV.h>

@interface Statusbar : CDVPlugin

@end

@implementation Statusbar

- (void)pluginInitialize
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 110000
    if (@available(iOS 11.0, *)) {
        NSLog(@"Quobis: setting statusbar over content");
        [self.webView.scrollView setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }
#endif
}

@end